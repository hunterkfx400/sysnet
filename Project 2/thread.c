/**
* @file mt-collatz.c
* @brief Implements the ThreadManager, which creates, dispatches and frees threads.
*
* @author Ben Avellone
* @author Hunter Hardy
*
*/

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "thread.h"
#include "mt-collatz.h"

/**
* @brief Creates a ThreadManager to help manage our threads
* @return A ThreadManager
*/
ThreadManager createThreadManager(int threads) {
    ThreadManager tm = malloc(sizeof(ThreadManager*));
    tm->threads = threads;
    tm->threadIDs = malloc(sizeof(pthread_t) * threads);

    return tm;
}

/**
* @brief Creates the threads managed by the ThreadManager tm with the given
* entry-point function and data.
*/
void createThreads(ThreadManager tm, void *function, void **data) {
    int i;
    pthread_attr_t defaultAttr;

    // Set default attributes
    pthread_attr_init(&defaultAttr);

    for (i = 0; i < tm->threads; i++)
        pthread_create(&tm->threadIDs[i], &defaultAttr, function, data[i]);

    pthread_attr_destroy(&defaultAttr);
}

/**
* @brief Joins on all threads managed by the ThreadManager tm
*/
void joinThreads(ThreadManager tm) {
    int i;
    for (i = 0; i < tm->threads; i++) {
        pthread_join(tm->threadIDs[i], NULL);
    }
}

/**
* @brief Frees memory of ThreadManager tm
* @return Null
*/
ThreadManager freeThreadManager(ThreadManager tm) {
    free(tm->threadIDs);
    free(tm);
    return NULL;
}
