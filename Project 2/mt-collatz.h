/**
* @file mt-collatz.h
* @brief Prototypes for our Collatz functions and the ThreadLocal struct
*
* @author Ben Avellone
* @author Hunter Hardy
*
*/

#include <time.h>
#include "thread.h"
#define NUM_COLLATZ_BINS 1000
#define BILLION 1E9

typedef struct thread_local {
	long long start;
	long long stop;
} *ThreadLocal;

int initCollatz(int, char **);
void readTime(struct timespec);
float benchmark(ThreadManager, ThreadLocal*);
long long isEven(long long);
long long collatzOdd(long long);
long long collatzEven(long long);
void *dispatch(void *);
long long nextCollatzNumber(long long, int);
void printBins();
void printStats(long long, int, float);
ThreadLocal createThreadLocal(int, int);
ThreadLocal freeThreadLocal(ThreadLocal);
ThreadLocal *createContexts(int);
ThreadLocal *freeContexts(ThreadLocal *, int);
