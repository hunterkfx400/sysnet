#include "parse.h"

// Constants
#define PROMPT_BUFFER_SIZE 256

// Prototypes
int promptLoop(char *);
void clearBuffer(char *);
int isEOF();
void stripNewline(char *);
int parseInput(char *);
int checkExit(Context ctx);
void createChild(Context ctx);
void executeContext(char **, char *, char *);
int redirect(char *, char *);
void waitForChildren();
