#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "parse.h"

Context parse(char *input) {
	char *str;
	Context ctx = newContext();

	// Tokenize input
	str = strtok(input," \t\n");
	while(str != NULL) {
		// Our special chars are of length 1
		if(strlen(str) == 1)
			processSpecial(str, ctx, input);
		else {
			inputArgument(str, ctx);
		}
		str = strtok(NULL," \t\n");
	}

	return ctx;
}

void processSpecial(char *str, Context context, char *input) {
	char *redirect;
	char cmp = str[0];

	// Check for output redirection
	if(cmp == '>') {
		redirect = strtok(NULL," \t\n");
		// Make sure this is valid input
		if(redirect != NULL)
			context->outputRedirect = redirect;
	}

	// Check for input redirection
	else if(cmp == '<') {
		redirect= strtok(NULL," \t\n");
		// Make sure this is valid input
		if(redirect != NULL)
			context->inputRedirect = redirect;
	}

	// Check for background flag
	else if(cmp == '&')
		context->background = 1;

	// This is not a special char
	else
		inputArgument(str, context);
}

Context newContext() {
	Context ctx = malloc(sizeof(Context*));
	ctx->inputRedirect = NULL;
	ctx->outputRedirect = NULL;
	ctx->background = 0;
	ctx->argumentCount = 0;
	ctx->argumentVector[0] = NULL;
	return ctx;
}

void inputArgument(char *argument, Context ctx) {
	if (ctx->argumentCount <= MAX_ARGS) {
		ctx->argumentVector[ctx->argumentCount++] = argument;
        ctx->argumentVector[ctx->argumentCount] = NULL;
	}
	else {
		fprintf(stderr, "Maximum number of arguments (%i) reached!\n", MAX_ARGS);
	}
}

void printContext(Context ctx) {
	int i = 0;
	char *arg;
	printf("Input: %s\n",ctx->inputRedirect);
	printf("Output: %s\n",ctx->outputRedirect);
	printf("Background: %i\n",ctx->background);
	printf("ArgCount: %i\n",ctx->argumentCount);
	while ((arg = ctx->argumentVector[i]) != NULL)
		printf("Arg[%i]: %s\n", i++, arg);
}
